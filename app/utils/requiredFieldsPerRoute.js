// const validVariable = require("../helper/validVariable");
const { checkBodyAttributes } = require("../helper");

module.exports = {
  "POST: /vehicle": (request) => {
    const body = request.body;
    const arrValues = [];
    Object.keys(body).map((key) => {
      arrValues.push(body[key]);
    });
    if (!checkBodyAttributes(arrValues)) {
      return false;
    } else {
      return true;
    }
  },
};
