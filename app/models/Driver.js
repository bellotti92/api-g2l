const mongoose = require("mongoose");
const { Schema } = mongoose;

const driverSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    dateOfBirth: {
      type: Date,
      min: "1950-01-01",
      max: "2002-01-01",
      required: true,
    },
    status: {
      type: Boolean,
      require: true,
      default: true,
    },
    vehicles: {
      require: false,
      type: [Schema.Types.ObjectId],
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("drivers", driverSchema);
