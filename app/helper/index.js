module.exports = {
  checkValidVariable: require('./validVariable'),
  checkBodyAttributes: require('./checkBodyAttributes'),
};
