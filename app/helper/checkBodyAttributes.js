const validVariable = require("./validVariable");
module.exports = (arrayOfValues) => {
  let validationResult = null;
  for (let element = 0; element < arrayOfValues.length; element++) {
    validationResult = validVariable(arrayOfValues[element]);
    if (!validationResult) {
      return validationResult;
    }
  }
  return validationResult;
};
