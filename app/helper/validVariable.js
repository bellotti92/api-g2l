const invalidArray = [undefined, null, "", "undefined", "null"];
module.exports = (variable) => !invalidArray.includes(variable);
