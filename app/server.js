require("dotenv").config();

const bodyParser = require("body-parser");
const cors = require("cors");
const express = require("express");
const helmet = require("helmet");
const morgan = require("morgan");
const mongoose = require("mongoose");
const routes = require("./routes");
const middleware = require("./middleware");
const config = require("../config");

const { PORT } = config;
const port = PORT || 8080;

mongoose
  .connect(
    "mongodb+srv://bellotti:B0ZQODNf8VvZOZEN@cluster0.xyps8.gcp.mongodb.net/logistics?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
    }
  )
  .then((result) => {
    console.log("Database Connected!");
  })
  .catch((error) => {
    console.log(error);
  });

const app = express();

app.get("/health", (_, response) =>
  response.send({ msg: "Everything is good!" })
);

app.use(
  morgan("combined"),
  helmet(),
  cors(),
  bodyParser.json(),
  bodyParser.urlencoded({ extended: true }),
  middleware.validateApp
);

app.use(routes);

app.listen(port, () => console.info(`Server is live on port: ${port}`));
