const { requireFieldsPerRoute } = require("../utils");

module.exports = (request, response, next) => {
  const { method, url: rawPath } = request;

  let path = rawPath.split("/");
  path = path.join("/");

  const parsedValidator = `${method}: ${path}`;

  if (
    requireFieldsPerRoute[parsedValidator] &&
    !requireFieldsPerRoute[parsedValidator](request)
  ) {
    response.status(400).send("Bad Request: Check body of the request!");
  } else {
    next();
  }
};
