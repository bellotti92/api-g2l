const app = (module.exports = require("express")());

const Vehicle = require("../../models/Vehicle");

app.delete("/:id", async (request, response) => {
  const { id: _id } = request.params;
  Vehicle.findByIdAndDelete({ _id })
    .then((result) => response.json(result))
    .catch((error) => response.status(400).json(error.message));
});
