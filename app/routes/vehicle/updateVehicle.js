const app = (module.exports = require("express")());

const { checkBodyAttributes } = require("../../helper");
const Vehicle = require("../../models/Vehicle");

app.put("/:id", async (request, response) => {
  const { id: _id } = request.params;
  const { ownerName, plate } = request.body;
  if (!checkBodyAttributes([ownerName, plate])) {
    return response.status(400).send("Bad request: Check body of the request!");
  }
  Vehicle.findByIdAndUpdate({ _id }, { ownerName, plate }, { new: true })
    .then((result) => response.json(result))
    .catch((error) => response.status(400).json(error.message));
});
