const app = (module.exports = require('express')());

app.use('/vehicle', require('./createVehicle'));
app.use('/vehicles', require('./getAllVehicles'));
app.use('/vehicle', require('./updateVehicle'));
app.use('/vehicle', require('./deleteVehicle'));
