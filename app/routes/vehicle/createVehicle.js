const app = (module.exports = require("express")());
const Vehicle = require("../../models/Vehicle");

app.post("/", async (request, response) => {
  const { ownerName, plate } = request.body;

  const newVehicle = new Vehicle({ ownerName, plate });

  newVehicle
    .save()
    .then((result) => response.json(result))
    .catch((error) => response.status(400).json(error.message));
});
