const app = (module.exports = require('express')());

const Vehicle = require('../../models/Vehicle');

app.get('/', async (request, response) => {
  Vehicle.find()
      .then((result) => response.json(result))
      .catch((error) => response.status(500).json(error));
});
