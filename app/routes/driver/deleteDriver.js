const app = (module.exports = require("express")());

const Driver = require("../../models/Driver");

app.delete("/:id", async (request, response) => {
  const { id: _id } = request.params;
  Driver.findByIdAndDelete({ _id })
    .then((result) => response.json(result))
    .catch((error) => response.status(400).json(error.message));
});
