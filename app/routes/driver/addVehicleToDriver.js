const app = (module.exports = require("express")());

const Driver = require("../../models/Driver");

app.post("/:driverId/:vehicleId", async (request, response) => {
  const { driverId, vehicleId } = request.params;
  const oldDriver = await Driver.findById(driverId);
  oldDriver.vehicles = [...oldDriver.vehicles, vehicleId];
  Driver.findByIdAndUpdate(driverId, oldDriver)
    .then((result) => response.json(result))
    .catch((error) => response.status(400).json(error.message));
});
