const app = (module.exports = require("express")());

const { checkBodyAttributes } = require("../../helper");
const Driver = require("../../models/Driver");

app.put("/:id", async (request, response) => {
  const { id: _id } = request.params;
  const { name, lastName, dateOfBirth, status } = request.body;
  if (!checkBodyAttributes([name, lastName, dateOfBirth, status])) {
    return response.status(400).send("Bad request");
  }
  const newData = {
    name,
    lastName,
    dateOfBirth,
  };
  if (status !== undefined) {
    newData.status = status;
  }
  Driver.findByIdAndUpdate({ _id }, newData, { new: true })
    .then((result) => response.json(result))
    .catch((error) => response.status(400).json(error.message));
});
