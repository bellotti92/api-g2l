const app = (module.exports = require('express')());

app.use('/driver', require('./createDriver'));
app.use('/drivers', require('./getAllDrivers'));
app.use('/driver', require('./updateDriver'));
app.use('/driver', require('./deleteDriver'));
app.use('/driver', require('./addVehicleToDriver'));
