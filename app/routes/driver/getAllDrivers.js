const app = (module.exports = require('express')());

const Driver = require('../../models/Driver');

app.get('/', async (request, response) => {
  Driver.find()
      .then((result) => response.json(result))
      .catch((error) => response.status(500).json(error));
});
