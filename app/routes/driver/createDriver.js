const app = (module.exports = require("express")());

const { checkBodyAttributes } = require("../../helper");
const Driver = require("../../models/Driver");

app.post("/", async (request, response) => {
  const { name, lastName, dateOfBirth, vehicles } = request.body;
  if (!checkBodyAttributes([name, lastName, dateOfBirth])) {
    return response.status(400).send("Bad request: Check the body!");
  }

  const newDriver = new Driver({ name, lastName, dateOfBirth, vehicles });

  newDriver
    .save()
    .then((result) => response.json(result))
    .catch((error) => response.status(400).json(error.message));
});
