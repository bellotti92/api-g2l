const app = (module.exports = require('express')());

app.get('/', async (request, response) => {
  response.json({
    status: 'UP',
    message: 'Everything is good!',
  });
});
