const app = (module.exports = require('express')());

app.use('/system', require('./system'));
app.use(require('./vehicle'));
app.use(require('./driver'));
